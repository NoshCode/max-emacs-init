;; toolkit for the more *fancy* stuff

;; add git bash as default shell (might need to tweak this for unix)
(defun run-win-ubuntu ()
  (interactive)
  (let ((shell-file-name "C:\\Program Files\\Git\\git-bash.exe" ))
    (shell "*ubuntu*"))
  )

(load (get-full-path "make-mode.el"))


(defvar org-journal-file "~/notes/engjournal.org"  
  "Path to OrgMode journal file.")  
(defvar org-journal-date-format "<%Y-%m-%d %a %I:%M %p>"  
  "Date format string for journal headings.")  

(defun org-journal-entry ()  
  "Create a new diary entry for today or append to an existing one."  
  (interactive)  
  (switch-to-buffer (find-file org-journal-file))  
  (widen)  
  (let ((today (format-time-string org-journal-date-format)))  
    (beginning-of-buffer)  
    (unless (org-goto-local-search-headings today nil t)  
      ((lambda ()   
	 (org-insert-heading)  
	 (insert today)  
	 (insert " "))))  
    (beginning-of-buffer)  
    (org-show-entry)  
    (org-narrow-to-subtree)  
    (end-of-buffer)))  

(global-set-key "\C-cj" 'org-journal-entry)

;; disable the emacs beeping when scrolling too far
(setq visible-bell 1)

;; remap the cursor control to vim-like commands

;; (global-set-key (kbd "M-j") 'backward-char) ; was indent-new-comment-line
;; (global-set-key (kbd "M-l") 'forward-char)  ; was downcase-word
;; (global-set-key (kbd "M-i") 'previous-line) ; was tab-to-tab-stop
;; (global-set-key (kbd "M-k") 'next-line) ; was kill-sentence
