;; this is where I keep all the nice things that make emacs easier to use

;; default into fullscreen because no one likes tiny ass screens
;; for unix
;;(custom-set-variables
;; '(initial-fame-alist (quote ((fullscreen . maximized)))))

;; for windows
(w32-send-sys-command 61488)

(when (version<="26.0.50" emacs-version )
  (global-display-line-numbers-mode))

;; remembers the cursor location in files
;; in windows
(desktop-save-mode 1)


;; enable default copy + paste keys
(cua-mode 1)

;; enable selection overwriting
(delete-selection-mode 1)

;; disable the row highlighting (overrides the colour scheme when enabled)
(global-hl-line-mode 0)

;; enabled the parenthesis highlighting
(show-paren-mode 1)

;; enable auto-refreshing files when they are changed outside the editor
(global-auto-revert-mode 1)

;; word wrap by word for long lines
(global-visual-line-mode 1)
