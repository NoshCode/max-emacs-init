;; ---------------------------------
;; package initialization
(require 'package)
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
                    (not (gnutls-available-p))))
       (proto (if no-ssl "http" "https")))
  (when no-ssl
    (warn "\
Your version of Emacs does not support SSL connections,
which is unsafe because it allows man-in-the-middle attacks.
There are two things you can do about this warning:
1. Install an Emacs version that does support SSL and be safe.
2. Remove this warning from your init file so you won't see it again."))
  ;; Comment/uncomment these two lines to enable/disable MELPA and ELPA Stable as desired
  (add-to-list 'package-archives (cons "melpa" (concat proto "://melpa.org/packages/")) t)
  ;;(add-to-list 'package-archives (cons "melpa-stable" (concat proto "://stable.melpa.org/packages/")) t)
  (when (< emacs-major-version 24)
    ;; For important compatibility libraries like cl-lib
    (add-to-list 'package-archives (cons "gnu" (concat proto "://elpa.gnu.org/packages/")))))
(package-initialize)

;; ---------------------------------
;; Custom shit for emacs.

;; need to include this so emacs doesn't lose the path
;; when jumping through loading files
(defun get-full-path (@file-relative-path)
  (concat (file-name-directory (or load-file-name buffer-file-name))
	  @file-relative-path)
  )


;; ---------------------------------
;; load the housekeeping items
(load (get-full-path "housekeeping.el"))


;; ---------------------------------
;; load the pretty theme crap
(load (get-full-path "theming.el"))


;; ---------------------------------
;; load your tools
(load (get-full-path "tools.el"))
